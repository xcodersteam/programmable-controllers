data:extend(
{
  {
    type = "item-group",
    name = "programmable-controllers",
    icon = "__ProgrammableControllers__/graphics/tech.png",
    inventory_order = "d",
    order = "e"
  },
  {
    type = "item-subgroup",
    name = "programmable-controllers-blocks",
    group = "programmable-controllers",
    order = "a"
  }
})